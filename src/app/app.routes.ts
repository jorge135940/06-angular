import { RouterModule, Routes } from "@angular/router";
import { InicioComponent } from "./components/inicio/inicio.component";
import { InformacionComponent } from "./components/informacion/informacion.component";
import { ContactosComponent } from "./components/contactos/contactos.component";
import { Opcion1Component } from "./components/opcion1/opcion1.component";
import { Opcion2Component } from "./components/opcion2/opcion2.component";
import { Opcion3Component } from "./components/opcion3/opcion3.component";
import { Opcion4Component } from "./components/opcion4/opcion4.component";
import { Opcion5Component } from "./components/opcion5/opcion5.component";



const APP_ROUTES: Routes = [
{ path: 'inicio', component: InicioComponent },	
{ path: 'informacion', component: InformacionComponent },
{ path: 'contactos', component: ContactosComponent },
{ path: 'opcion1', component: Opcion1Component },
{ path: 'opcion2', component: Opcion2Component },
{ path: 'opcion3', component: Opcion3Component },
{ path: 'opcion4', component: Opcion4Component },
{ path: 'opcion5', component: Opcion5Component },

{ path: '**', pathMatch: 'full', redirectTo: 'inicio' },
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);
